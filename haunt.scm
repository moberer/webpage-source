(use-modules (haunt artifact)
             (haunt asset)
             (haunt site)
             (haunt builder assets)
             (haunt builder blog)
             (haunt builder atom)
             (haunt html)
             (haunt post)
             (haunt reader)
             (haunt reader commonmark)
             (srfi srfi-1)
             (srfi srfi-19))

(define (current-year)
  (number->string (date-year (current-date))))

(define (site-author site)
  (assq-ref (site-default-metadata site) 'author))

(define (dependency name link license license-link purpose)
  `((a (@ (href ,link)) ,name)  
    " - Licensed under "
    (a (@ (href ,license-link)) ,license)
    " - " ,purpose))


(define (prescheme-layout site title body)
  `((doctype "html")
    (html (@ (lang "en"))
          (head
           (meta (@ (charset "utf-8")))
           (meta (@ (name "viewport") (content "width=device-width, initial-scale=1")))
           (meta (@ (http-equiv "Content-Security-Policy") 
                    (@ (content "default-src 'self'; script-src 'none'"))))
           (title ,(if title
                       (string-append title " — " (site-title site))
                       (site-title site)))
           (link (@ (rel "stylesheet") (href "/css/site.css")))
           (link (@ (rel "stylesheet") (href "/css/normalize.css")))
           (link (@ (rel "icon") (type "image/x-icon") (href "assets/favicon.png") (sizes "16x16")))
           (link (@ (rel "icon") (type "image/x-icon") (href "assets/favicon-32.png") (sizes "32x32"))))
          (body
           (header
            (nav
             (h1 (a (@ (href "/")) "oberaigner.info"))
             (ul (li (a (@ (href "/projects.html")) "Projects"))
                 (li (a (@ (href "/about.html")) "About")))))
           ,body
           (footer
            "© " ,(current-year) ", " ,(site-author site) "."
            (br)
            "Futher information about this site, its license, and used software can be found on the "
            (a (@ (href "/about.html")) "about page") "."

               )))))

(define prescheme-theme
  (theme #:name "custom-theme"
         #:layout prescheme-layout))

(define (static-page input-file output-file )
  (lambda (site posts)
    (let* ((reader (or (find (lambda (reader)
                               (reader-match? reader input-file))
                             (site-readers site))
                       (error "No reader matched" input-file)))
           (post (read-post reader input-file (site-default-metadata site))))
      (serialized-artifact output-file
                           (with-layout prescheme-theme site
                                        (post-ref post 'title)
                                        (post-sxml post))
                           sxml->html))))

(define (static-directory-all input-directory output-directory)
  (lambda (site posts) 
    (directory-assets
      input-directory
      (lambda (x) #t) ;; copy 'all' files
      output-directory)))


(site #:title "oberaigner.info"
      #:domain "oberaigner.info"
      #:default-metadata
      '((author . "Max Oberaigner"))
      #:readers (list commonmark-reader)
      #:builders (list (static-page "pages/index.md"     "index.html")
                       (static-page "pages/projects.md"  "projects.html")
                       (static-page "pages/about.md"     "about.html")
                       (static-directory-all "rootfiles" "/")
                       (static-directory     "css")
                       (static-directory     "assets")))
