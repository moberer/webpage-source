#!/bin/bash

set -eox pipefail

haunt build

cd site

git init .
git add -A
git commit -m "Current Page State"
git remote add origin git@codeberg.org:moberer/pages.git
git push --set-upstream origin main --force

cd ..
