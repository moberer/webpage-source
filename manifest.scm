(use-modules (gnu packages base)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (guix packages)
             (guix profiles)
             (guix utils))


(packages->manifest
 (list guile-syntax-highlight
       haunt))
