title: About
date: 2024-04-30 20:00
tags: about
---

# About this Website

## Copyright and License

This website and its contents are licensed under [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html#license-text).\
The full source code is available [on Codeberg](https://codeberg.org/moberer/webpage-source).


## Attributions

This site uses and is built using the following tools:

### Guile Haunt
- [Guile Haunt](https://dthompson.us/projects/haunt.html), by David Thompson et.al.;
- Licensed under [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html#license-text);
- Templating/Site Generation;

### Normalize.css
- [Normalize.css](https://necolas.github.io/normalize.css), by Nicolas Gallagher et.al.;
- Licensed under [MIT](https://github.com/necolas/normalize.css/blob/master/LICENSE.md);
- Advanced CSS Rules;

### Pre-Scheme Website
- [Pre-Scheme Website](https://prescheme.org/), by Andrew Whatson;
- Licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0);
- Used as a base for CSS and Site Generation Code;



## Small Disclosure Obligation 

Information about this site according to the Austrian small disclosure obligation 
("kleine Offenlegungspflicht"):

- **Operator**: Max Oberaigner
- **Location**: Piesendorf, Austria
- **Purpose**: Personal Webpage, highlighting software projects;
