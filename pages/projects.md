title: Projects
date: 2024-04-30 20:00
tags: projects
---


# Personal Projects

## Libelle

A small, functional programming language, implemented with a bytecode VM.

- [Source Code](https://codeberg.org/Libelle/Libelle)

## Oncer

A utility to avoid running programs more than once.

- [Source Code](https://codeberg.org/moberer/oncer)
- [crates.io](https://crates.io/crates/oncer)

## Notato

The smallest viable notes app. - Very much in-progress.

- [Source Code](https://codeberg.org/moberer/Notato.git)

## AutoKeyMute

A utility for people with excessively loud keyboards.\
Automatically mutes your microphone while you are typing.

- [Source Code](https://gitlab.com/moberer/autokeymute)

## This Website

This website was made using Guile Haunt, a nice, small static site generator written in Guile Scheme.

- [Source Code](https://codeberg.org/moberer/webpage-source)
- [Generated Code](https://codeberg.org/moberer/pages)
- [Link to (this) site](https://oberaigner.info)
